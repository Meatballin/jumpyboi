using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyProjectile : MonoBehaviour
{
    private float maxDuration = 3.0f;
    

    // Update is called once per frame
    void Update()
    {
        maxDuration -= Time.deltaTime;
        if(maxDuration <= 0)
        {
            Destroy(gameObject);
        }
    }
}
