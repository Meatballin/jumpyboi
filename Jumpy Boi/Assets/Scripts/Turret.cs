using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    public GameObject projectile;
    private float startTime = 1.0f;
    private float repeatRate = 1.5f;
    public Transform projectilePosition;

    void Start()
    {
        InvokeRepeating("spawnProjectile", startTime, repeatRate);
    }

   
    void spawnProjectile()
    {

        Instantiate(projectile, projectilePosition.transform.position, transform.rotation);
    }

   
}
