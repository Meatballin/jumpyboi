using UnityEngine;

public class RopeAutoSwing : MonoBehaviour
{
    public Transform negXBoundary;
    public Transform posXBoundary;
    public Rigidbody2D ropeRb;
    [SerializeField] bool swingLeft = true;
    [SerializeField] bool swingRight = false;
    [SerializeField] float pushForce = 1.2f;
    public float currentXPos;

    private void Start()
    {
        ropeRb = GetComponent<Rigidbody2D>();
        
        
    }
    void Update()
    {
       
        if(swingLeft && (currentXPos > negXBoundary.transform.localPosition.x))
        {
            currentXPos = this.transform.localPosition.x;
            ropeRb.AddForce(Vector2.left * pushForce, ForceMode2D.Force);
            if (currentXPos < negXBoundary.transform.localPosition.x)
            {
                swingLeft = false;
                swingRight = true;
            }
        }
        else if(swingRight && (currentXPos < posXBoundary.transform.localPosition.x))
        {
            currentXPos = this.transform.localPosition.x;
            ropeRb.AddForce(Vector2.right * pushForce, ForceMode2D.Force);
            if (currentXPos > posXBoundary.transform.localPosition.x)
            {
                swingRight = false;
                swingLeft = true;
            }
        }
    
        

    }
}
