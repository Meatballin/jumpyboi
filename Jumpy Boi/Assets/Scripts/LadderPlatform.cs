using UnityEngine;

public class LadderPlatform : MonoBehaviour
{
    private PlatformEffector2D effector;
    public float waitTime;
    
    void Start()
    {
        effector = GetComponent<PlatformEffector2D>();
    }

   
    void Update()
    {
        
        
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            
            if(waitTime <= 0)
            {
                effector.rotationalOffset = 180f;
                waitTime = 0.5f;
            }
            
        }
        else
        {
            waitTime -= Time.deltaTime;
        }

        if (Input.GetButtonDown("Jump") || Input.GetKeyDown(KeyCode.UpArrow))
        {
            effector.rotationalOffset = 0f;
        }
    }
}
